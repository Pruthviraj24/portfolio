
let arryOfArticles = document.getElementsByTagName("article");
let bodyEl = document.getElementById("body")
let buttonSun = document.getElementById("btn-sun")
let buttonMoon = document.getElementById("btn-moon")
let skillsList = document.getElementById("skills")
let anchors = document.getElementsByTagName("a")
let buttons = document.getElementsByTagName("button")


function changeColor() {

    for(let eachArticle of arryOfArticles){
        eachArticle.classList.toggle("dark-background-articles")
    }

    bodyEl.classList.toggle("dark-background-body")
    buttonSun.classList.toggle("display-mode-sun")
    buttonMoon.classList.toggle("display-mode-sun")


    for(let skill of skillsList.children){
        skill.classList.toggle("dark-background-articles")
    }

    for(let button of buttons){
        button.classList.toggle("button-style")
    }

    for(let anchor of anchors){
        anchor.classList.toggle("anchor")
    }

}